use std::borrow::Cow;

use hyper::Method;
use serde::Serialize;
use url::Url;
use uuid::Uuid;

use crate::{models::{
    Type, 
    Flow, channel::{ChannelDetails, ChannelList}
}, Endpoint};

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ChannelCreationRequest {
    name: String,
    ttype: Type,
    flow: Flow,
    pub monthly_consumption_limit: Option<u32>,
    pub default_callback_url_status: Option<Url>,
    pub default_callback_url_mo: Option<Url>,
}

impl ChannelCreationRequest {
    pub fn new(name: &str, ttype: Type, flow: Flow) -> Self {
        Self {
            name: name.to_string(),
            ttype,
            flow,
            monthly_consumption_limit: None,
            default_callback_url_status: None,
            default_callback_url_mo: None,
        }
    }
}

impl Endpoint for ChannelCreationRequest {
    type Output = ChannelDetails;

    fn method(&self) -> Method {
        Method::POST
    }

    fn path(&self) -> Cow<str> {
        "/channels".into()
    }
}


#[derive(Debug, Clone, Serialize)]
pub struct ChannelsListRequest {
    pub page: u16,
    pub page_size: u32,    
}

impl ChannelsListRequest {
    pub fn new() -> Self {
        Self {
            page: 1,
            page_size: 10,
        }
    }
}

impl Endpoint for ChannelsListRequest {
    type Output = ChannelList;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        "/channels".into()
    }
}


#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ChannelDetailsRequest {
    #[serde(skip_serializing)]
    channel_id: Uuid,
}

impl ChannelDetailsRequest {
    pub fn new(channel_id: Uuid) -> Self {
        Self {
            channel_id
        }
    }
}

impl Endpoint for ChannelDetailsRequest {
    type Output = ChannelDetails;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        format!("/channels/{}", self.channel_id).into()
    }
}

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ChannelUpdateRequest {
    #[serde(skip_serializing)]
    channel_id: Uuid,
    pub name: Option<String>,
    pub monthly_consumption_limit: Option<u32>,
    pub default_callback_url_status: Option<Url>,
    pub default_callback_url_mo: Option<Url>,
}

impl ChannelUpdateRequest {
    pub fn new(channel_id: Uuid) -> Self {
        Self {
            channel_id,
            name: None,
            monthly_consumption_limit: None,
            default_callback_url_status: None,
            default_callback_url_mo: None,
        }
    }
}

impl Endpoint for ChannelUpdateRequest {
    type Output = ChannelDetails;

    fn method(&self) -> Method {
        Method::PATCH
    }

    fn path(&self) -> Cow<str> {
        format!("/channels/{}", self.channel_id).into()
    }
}
