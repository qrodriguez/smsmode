use std::{collections::BTreeMap as Map, borrow::Cow};
use chrono::{DateTime, Utc, Duration};
use hyper::Method;
use serde::Serialize;
use uuid::Uuid;

use crate::{
    models::consumptions::{
        ConsumptionDetails, 
        ConsumptionList
    }, 
    Endpoint
};

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ConsumptionsListRequest {
    pub start_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
    pub page: u16,
    pub page_size: u32,
    #[serde(skip_serializing_if = "Map::is_empty")]
    pub search_by: Map<String, String>,
}

impl ConsumptionsListRequest {
    pub fn new() -> Self {
        Self {
            start_date: Utc::now() - Duration::days(90),
            end_date: Utc::now(),
            page: 1,
            page_size: 10,
            search_by: Map::new(),
        }
    }
}

impl Endpoint for ConsumptionsListRequest {
    type Output = ConsumptionList;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        "/consumptions".into()
    }
}


#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ConsumptionDetailsRequest {
    #[serde(skip_serializing)]
    consumption_id: Uuid,
    #[serde(skip_serializing_if = "Map::is_empty")]
    pub search_by: Map<String, String>,
    pub group_by: String,
    pub detail_statuses: bool
}

impl ConsumptionDetailsRequest {
    pub fn new(consumption_id: Uuid) -> Self {
        Self {
            consumption_id,
            search_by: Map::new(),
            group_by: "mccMnc".to_owned(),
            detail_statuses: false,
        }
    }
}

impl Endpoint for ConsumptionDetailsRequest {
    type Output = ConsumptionDetails;

    fn method(&self) -> hyper::Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        format!("/consumptions/{}", self.consumption_id).into()
    }
}