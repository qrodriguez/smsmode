use std::borrow::Cow;
use hyper::Method;
use serde::Serialize;

use crate::{
    models::lookup::NumberLookup, 
    Endpoint
};

#[derive(Debug, Default, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct NumberLookupRequest {
    #[serde(skip_serializing)]
    pub msisdn: String,
}

impl NumberLookupRequest {
    pub fn new(msisdn: String) -> Self {
        Self {
            msisdn
        }
    }
}

impl Endpoint for NumberLookupRequest {
    type Output = NumberLookup;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        format!("/lookup/{}", self.msisdn).into()
    }
}
