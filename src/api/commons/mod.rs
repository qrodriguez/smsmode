#[cfg(feature = "channel")]
pub mod channel;
#[cfg(feature = "consumption")]
pub mod consumption;
#[cfg(feature = "lookup")]
pub mod lookup;
#[cfg(feature = "organization")]
pub mod organization;

use std::future::Future;

use crate::{
    SmsMode, 
    models::{organizations::{
        OrganizationDetails, 
        OrganizationList
    }, consumptions::{ConsumptionList, ConsumptionDetails}, lookup::NumberLookup}, 
    errors::Result
};

use self::{organization::{
    OrganizationDetailsRequest, 
    OrganizationsListRequest
}, consumption::{ConsumptionsListRequest, ConsumptionDetailsRequest}, lookup::NumberLookupRequest};

#[cfg(feature = "channel")]
pub struct ChannelHandler<'commons> {
    sms_mode: SmsMode<'commons>,
}

#[cfg(feature = "channel")]
impl<'commons> ChannelHandler<'commons> {
    pub fn new(sms_mode: SmsMode<'commons>) -> Self {
        Self {
            sms_mode
        }
    }



}


#[cfg(feature = "consumption")]
pub struct ConsumptionHandler<'commons> {
    sms_mode: SmsMode<'commons>,
}

#[cfg(feature = "consumption")]
impl<'commons> ConsumptionHandler<'commons> {
    pub fn new(sms_mode: SmsMode<'commons>) -> Self {
        Self {
            sms_mode
        }
    }

    pub fn list(&'commons self, request: ConsumptionsListRequest) -> impl Future<Output = Result<ConsumptionList>> + 'commons {
        self.sms_mode.common_client.request::<ConsumptionsListRequest>(request)
    }

    pub fn details(&'commons self, request: ConsumptionDetailsRequest) -> impl Future<Output = Result<ConsumptionDetails>> + 'commons {
        self.sms_mode.common_client.request::<ConsumptionDetailsRequest>(request)
    }
}

#[cfg(feature = "lookup")]
pub struct LookupHandler<'commons> {
    sms_mode: SmsMode<'commons>,
}

#[cfg(feature = "lookup")]
impl<'commons> LookupHandler<'commons> {
    pub fn new(sms_mode: SmsMode<'commons>) -> Self {
        Self { 
            sms_mode 
        }
    }

    pub fn number(&'commons self, request: NumberLookupRequest) -> impl Future<Output = Result<NumberLookup>> + 'commons {
        self.sms_mode.common_client.request(request)
    }
}

#[cfg(feature = "organization")]
pub struct OrganizationHandler<'commons> {
    sms_mode: SmsMode<'commons>
}

#[cfg(feature = "organization")]
impl<'commons> OrganizationHandler<'commons> {
    pub fn new(sms_mode: SmsMode<'commons>) -> Self {
        Self { 
            sms_mode 
        }
    }

    pub fn list(&'commons self, request: OrganizationsListRequest) -> impl Future<Output = Result<OrganizationList>> + 'commons {
        self.sms_mode.common_client.request::<OrganizationsListRequest>(request)
    }

    pub fn details(&'commons self, request: OrganizationDetailsRequest) -> impl Future<Output = Result<OrganizationDetails>> + 'commons {
        self.sms_mode.common_client.request::<OrganizationDetailsRequest>(request)
    } 
}
