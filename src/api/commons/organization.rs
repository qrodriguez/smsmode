use std::borrow::Cow;

use hyper::Method;
use serde::Serialize;
use uuid::Uuid;

use crate::{ 
    models::organizations::{
        OrganizationDetails, 
        OrganizationList
    }, Endpoint
};

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct OrganizationsListRequest {
    pub page: u16,
    pub page_size: u32,
}

impl OrganizationsListRequest {
    pub fn new() -> Self {
        Self {
            page: 1,
            page_size: 10,
        }
    }
}

impl Endpoint for OrganizationsListRequest {
    type Output = OrganizationList;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        "/organisations".into()
    }
}


#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct OrganizationDetailsRequest {
    #[serde(skip_serializing)]
    organization_id: Uuid,
}

impl OrganizationDetailsRequest {
    pub fn new(organization_id: Uuid) -> Self {
        Self {
            organization_id
        }
    }
}

impl Endpoint for OrganizationDetailsRequest {
    type Output = OrganizationDetails;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        format!("/organisations/{}", self.organization_id).into()
    }
}
