use std::{borrow::Cow, vec, collections::BTreeMap as Map};

use chrono::{DateTime, Duration, Utc};
use hyper::Method;
use serde::Serialize;
use url::Url;
use uuid::Uuid;

use crate::{
    models::{
        campaigns::{
            CampaignDetails, 
            CampaignList, 
            CampaignMessageList
        },
        Body, 
        Recipient, 
    }, 
    Endpoint,
};

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SendCampaignRequest {
    pub recipients: Vec<Recipient>,
    pub body: Body,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from: Option<String>,
    pub send_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ref_client: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub callback_url_status: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub callback_url_mo: Option<Url>,
}

impl Default for SendCampaignRequest {
    fn default() -> Self {
        Self {
            recipients: vec![],
            body: Body::default(),
            from: None,
            send_date: Utc::now(),
            end_date: Utc::now() + Duration::days(7),
            ref_client: None,
            callback_url_status: None,
            callback_url_mo: None,
        }
    }
}

impl Endpoint for SendCampaignRequest {
    type Output = CampaignDetails;

    fn method(&self) -> Method {
        Method::POST
    }

    fn path(&self) -> Cow<str> {
        "/campaigns".into()
    }
}

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct CampaignListRequest {
    pub start_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
    pub page: u16,
    pub page_size: u32,
    #[serde(skip_serializing_if = "Map::is_empty")]
    pub search_by: Map<String, String>,
}

impl CampaignListRequest {
    pub fn new() -> Self {
        Self {
            start_date: Utc::now() - Duration::days(10),
            end_date: Utc::now(),
            page: 1,
            page_size: 10,
            search_by: Map::new(),
        }
    }
}

impl Endpoint for CampaignListRequest {
    type Output = CampaignList;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        "/campains".into()
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct CampaignDetailsRequest {
    #[serde(skip_serializing)]
    campaign_id: Uuid,
}

impl CampaignDetailsRequest {
    pub fn new(campaign_id: Uuid) -> Self {
        Self {
            campaign_id,
        }
    }
}


impl Endpoint for CampaignDetailsRequest {
    type Output = CampaignDetails;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        format!("/campaigns/{}", self.campaign_id).into()
    }
}

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct CampaignMessagesListRequest {
    #[serde(skip_serializing)]
    campaign_id: Uuid,
    pub page: u16,
    pub page_size: u32,
    #[serde(skip_serializing_if = "Map::is_empty")]
    pub search_by: Map<String, String>,
}

impl CampaignMessagesListRequest {
    pub fn new(campaign_id: Uuid) -> Self {
        Self {
            campaign_id,
            page: 1,
            page_size: 10,
            search_by: Map::new()
        }
    }
}

impl Endpoint for CampaignMessagesListRequest {
    type Output = CampaignMessageList;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        format!("/campaigns/{}/messages", self.campaign_id).into()
    }
}
