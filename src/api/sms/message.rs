use std::{borrow::Cow, collections::BTreeMap as Map};
use chrono::{DateTime, Utc, Duration};
use hyper::Method;
use serde::Serialize;
use url::Url;
use uuid::Uuid;

use crate::{
    models::{
        Recipient, 
        Body,  
        messages::{
            MessageDetails, 
            MessageList
        }
    }, 
    Endpoint
};

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SendMessageRequest {
    pub recipient: Recipient,
    pub body: Body,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from: Option<String>,
    pub sent_date: DateTime<Utc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ref_client: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub callback_url_status: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub callback_url_mo: Option<Url>,
}

impl SendMessageRequest {
    pub fn new() -> Self {
        Self {
            recipient: Recipient::default(),
            body: Body::default(),
            from: None,
            sent_date: Utc::now(),
            ref_client: None,
            callback_url_status: None,
            callback_url_mo: None,
        }
    }
}

impl Endpoint for SendMessageRequest {
    type Output = MessageDetails;

    fn method(&self) -> Method {
        Method::POST
    }

    fn path(&self) -> Cow<str> {
        "/messages".into()
    }
}

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct MessageDetailsRequest {
    pub start_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
    pub page: u16,
    pub page_size: u32,
    #[serde(skip_serializing_if = "Map::is_empty")]
    pub search_by: Map<String, String>,
}

impl MessageDetailsRequest {
    pub fn new() -> Self {
        Self {
            start_date: Utc::now() - Duration::days(10),
            end_date: Utc::now(),
            page: 1,
            page_size: 10,
            search_by: Map::new(),
        }
    }
}

impl Endpoint for MessageDetailsRequest {
    type Output = MessageDetails;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        "/messages".into()
    }
}

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct MessageListRequest {
    message_id: Uuid,
}

impl MessageListRequest {
    pub fn new(message_id: Uuid) -> Self {
        Self {
            message_id
        }
    }
}

impl Endpoint for MessageListRequest {
    type Output = MessageList;

    fn method(&self) -> Method {
        Method::GET
    }

    fn path(&self) -> Cow<str> {
        format!("/messages/{}", self.message_id).into()
    }
}

