#[cfg(feature = "message")]
pub mod message;
#[cfg(feature = "campaign")]
pub mod campaign;

use std::future::Future;

use crate::{
    models::{
        messages::{
            MessageDetails, 
            MessageList
        }, 
        campaigns::{
            CampaignDetails, 
            CampaignList, 
            CampaignMessageList
        }
    }, 
    errors::Result, 
    SmsMode
};

use self::{
    message::{
        SendMessageRequest, 
        MessageDetailsRequest, 
        MessageListRequest
    }, 
    campaign::{
        SendCampaignRequest, 
        CampaignListRequest, 
        CampaignDetailsRequest, 
        CampaignMessagesListRequest
    }
};

#[cfg(feature = "message")]
pub struct MessageHandler<'sms> {
    sms_mode: SmsMode<'sms>
}

#[cfg(feature = "message")]
impl<'sms> MessageHandler<'sms> {
    pub fn new(sms_mode: SmsMode<'sms>) -> Self {
        Self { sms_mode }
    }

    pub fn send(&'sms self, request: SendMessageRequest) -> impl Future<Output = Result<MessageDetails>> + 'sms {
        self.sms_mode.sms_client.request::<SendMessageRequest>(request)
    }  

    pub fn list(&'sms self, request: MessageListRequest) -> impl Future<Output = Result<MessageList>> + 'sms {
        self.sms_mode.sms_client.request::<MessageListRequest>(request)
    }

    pub fn details(&'sms self, request: MessageDetailsRequest) -> impl Future<Output = Result<MessageDetails>> + 'sms {
        self.sms_mode.sms_client.request::<MessageDetailsRequest>(request)
    }

}

#[cfg(feature = "campaign")]
pub struct CampaignHandler<'sms> {
    sms_mode: SmsMode<'sms>
}

#[cfg(feature = "campaign")]
impl<'sms> CampaignHandler<'sms> {
    pub fn new(sms_mode: SmsMode<'sms>) -> Self {
        Self { sms_mode }
    }
} 


