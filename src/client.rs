use hyper::{
    Client as HttpClient, 
    client::HttpConnector, 
    Request, 
    header::{CONTENT_TYPE, ACCEPT}, 
    Body
};
use hyper_tls::HttpsConnector;
use serde::Serialize;
use url::Url;

use crate::{errors::Result, Endpoint};

const X_API_KEY: &str = "X-Api-Key";

#[derive(Debug, Clone)]
pub struct Client<'a> {
    http_client: HttpClient<HttpsConnector<HttpConnector>>,
    base_uri: &'a str,
    api_key: &'a str,
    timeout: u16
}

impl<'a> Client<'a> {
    pub fn new(base_uri: &'a str, api_key: &'a str, timeout: u16) -> Self {
        Self { 
            http_client: HttpClient::builder().build(HttpsConnector::new()), 
            base_uri, 
            api_key, 
            timeout 
        }
    }

    pub async fn request<T>(&self, endpoint: T) -> Result<<T as Endpoint>::Output>
    where
        T: Endpoint + Serialize
    {
        let url = Url::parse(&format!("{}/{}", self.base_uri, endpoint.path()))?;

        // if let Some(qs) = query {
        //     url.join(&serde_urlencoded::to_string(&qs)?)?;
        // }

        let request = Request::builder()
            .uri(url.as_ref())
            .header(ACCEPT, "application/json")
            .header(CONTENT_TYPE, "application/json")
            .header(X_API_KEY, self.api_key)
            .method(endpoint.method())
            .body(encode_body::<T>(Some(endpoint))?)?;

        let response = self.http_client.request(request).await?;
        let bytes = hyper::body::to_bytes(response.into_body()).await?;

        Ok(serde_json::from_slice::<T::Output>(&bytes)?)
    }

}

fn encode_body<S>(body: Option<S>) -> Result<Body>
where
    S: Serialize
{
    let payload = match body.map(|val| serde_json::to_string(&val)) {
        Some(Ok(res)) => Ok(Some(res)),
        Some(Err(err)) => Err(err.into()),
        None => Ok(None)
    };

    payload.map(|val| {
        val
            .map(|content| content.into())
            .unwrap_or_else(Body::empty)
    })
}