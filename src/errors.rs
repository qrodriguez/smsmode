use thiserror::Error as ThisError;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(ThisError, Debug)]
pub enum Error {
    #[error(transparent)]
    InvalidUrl(#[from] url::ParseError),
    #[error(transparent)]
    InvalidJson(#[from] serde_json::Error),
    #[error(transparent)]
    InvalidQueryString(#[from] serde_urlencoded::ser::Error),
    #[error(transparent)]
    HttpError(#[from] hyper::http::Error),
    #[error(transparent)]
    NetworkError(#[from] hyper::Error),
}