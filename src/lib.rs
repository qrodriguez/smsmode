mod client;
pub mod errors;
pub mod models;
pub mod api;

use std::borrow::Cow;

use api::{ 
    commons::{
        OrganizationHandler, 
        ChannelHandler, 
        ConsumptionHandler, 
        LookupHandler
    }, 
    sms::{
        MessageHandler, 
        CampaignHandler
    }
};
use client::Client;
use hyper::Method;
use serde::de::DeserializeOwned;

const SMS_API_URI: &str = "https://rest.smsmode.com/sms/v1";
const COMMON_API_URI: &str = "https://rest.smsmode.com/commons/v1";


pub struct SmsMode<'a> {
    common_client: Client<'a>, 
    sms_client: Client<'a>
}

impl<'a> SmsMode<'a> {
    pub fn new(api_key: &'a str, timeout: u16) -> Self {
        Self {
            common_client: Client::new(COMMON_API_URI, api_key, timeout),
            sms_client: Client::new(SMS_API_URI, api_key, timeout)
        }
    }

    #[cfg(feature = "message")]
    pub fn message(self) -> MessageHandler<'a> {
        MessageHandler::new(self)
    }

    #[cfg(feature = "campaign")]
    pub fn campaign(self) -> CampaignHandler<'a> {
        CampaignHandler::new(self)
    }

    #[cfg(feature = "channel")]
    pub fn channel(self) -> ChannelHandler<'a> {
        ChannelHandler::new(self)
    }

    #[cfg(feature = "consumption")]
    pub fn consumption(self) -> ConsumptionHandler<'a> {
        ConsumptionHandler::new(self)
    }

    #[cfg(feature = "lookup")]
    pub fn lookup(self) -> LookupHandler<'a> {
        LookupHandler::new(self)
    }

    #[cfg(feature = "organization")]
    pub fn organization(self) -> OrganizationHandler<'a> {
        OrganizationHandler::new(self)
    }


}

pub trait Endpoint {

    type Output: DeserializeOwned;

    fn path(&self) -> Cow<str>;

    fn method(&self) -> Method;

}