use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use url::Url;
use uuid::Uuid;

use super::{
    Channel, 
    Body, 
    Links, 
    messages::MessageDetails
};

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampaignList {
    start_date: DateTime<Utc>,
    end_date: DateTime<Utc>,
    page: u8,
    page_size: u8,
    count: u16,
    total_count: u16,
    links: Links,
    items: Vec<CampaignDetails>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampaignMessageList {
    page: u16,
    page_size: u16,
    count: u32,
    total_count: u32,
    links: Links,
    items: Vec<MessageDetails>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampaignDetails {
    campaign_id: Uuid,
    accepted_at: DateTime<Utc>,
    send_date: DateTime<Utc>,
    end_date: DateTime<Utc>,
    channel: Channel,
    #[serde(rename(deserialize = "type"))]
    _type: String,
    from: String,
    body: Body,
    ref_client: Option<String>,
    callback_url_status: Option<Url>,
    callback_url_mo: Option<Url>,
    href: Url,
}