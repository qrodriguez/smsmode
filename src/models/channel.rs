use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use url::Url;
use uuid::Uuid;

use super::{
    Links, 
    Type, 
    Flow
};

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChannelList {
    page: u16,
    page_size: u32,
    count: u32,
    total_count: u32,
    links: Links,
    items: Vec<ChannelDetails>,
}


#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChannelDetails {
    channel_id: Uuid,
    organisation_id: Uuid,
    name: String,
    #[serde(rename(deserialize = "type"))]
    ttype: Type,
    flow: Flow,
    default_from_field: String,
    from_field_list: Vec<String>,
    monthly_consumption: u32,
    monthly_consumption_limit: u32,
    archive_period: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    default_callback_url_status: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    default_callback_url_mo: Option<Url>,
    creation_date: DateTime<Utc>,
    href: Url,
}