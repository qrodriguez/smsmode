use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use url::Url;
use uuid::Uuid;

use super::{Channel, lookup::NumberLookup, Price};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ConsumptionList {

}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ConsumptionDetails {
    consumption_id: Uuid,
    channel: Channel,
    ttype: String,
    start_date: DateTime<Utc>,
    items: Vec<Items>,
    href: Url,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Items {
    lookup: NumberLookup,
    quantity: u32,
    price: Price,
    statuses: Vec<Status>
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Status {
    value: String,
    quantity: u32,
}