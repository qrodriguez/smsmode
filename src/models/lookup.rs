use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct NumberLookup {
    mcc_mnc: String,
    mcc: String,
    mnc: String,
    country: String,
    country_prefix: String,
    iso_country_code: String,
    network: String,
}