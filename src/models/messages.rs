use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use url::Url;
use uuid::Uuid;

use super::{
    Links, 
    Channel, 
    Recipient, 
    Body, 
    Price, 
    Status, 
    Direction
};

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MessageList {
    start_date: DateTime<Utc>,
    end_date: DateTime<Utc>,
    page: u16,
    page_size: u32,
    count: u32,
    total_count: u32,
    links: Links,
    items: Vec<MessageDetails>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MessageDetails {
    message_id: Uuid,
    campaign_id: Option<Uuid>,
    accepted_at: DateTime<Utc>,
    send_date: Option<DateTime<Utc>>,
    channel: Channel,
    #[serde(rename(deserialize = "type"))]
    ttype: String,
    direction: Direction,
    recipient: Recipient,
    from: String,
    body: Body,
    price: Option<Price>,
    status: Status,
    ref_client: Option<String>,
    callback_url_status: Option<Url>,
    callback_url_mo: Option<Url>,
    href: Url,
}
