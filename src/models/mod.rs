pub mod messages;
pub mod campaigns;
pub mod organizations;
pub mod channel;
pub mod consumptions;
pub mod lookup;

use std::collections::BTreeMap as Map;
use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use url::Url;
use uuid::Uuid;

use self::lookup::NumberLookup;

#[derive(Debug, Default, Clone, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Encoding {
    #[default] Gms7,
    Unicode,
}

#[derive(Debug, Default, Clone, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Direction {
    #[default] Mt,
    Mo,
}

#[derive(Debug, Default, Clone, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Type {
    #[default] Sms,
    Whatsapp,
    Rcs,
}

#[derive(Debug, Default, Clone, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Flow {
    #[default] Marketing,
    Transactionnal,
    Otp,
}


#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Links {
    first: Url,
    last: Url,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Channel {
    channel_id: Uuid,
    name: String,
    #[serde(rename(deserialize = "type"))]
    ttype: Type,
    flow: Flow,
}

#[derive(Debug, Default, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Recipient {
    pub to: String,
    #[serde(skip_serializing_if = "Map::is_empty")]
    pub parameters: Map<String, String>
}

#[derive(Debug, Default, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Body {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub text: Option<String>,
    pub encoding: Encoding,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub message_part_count: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub length: Option<u16>,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Price {
    amount: f32,
    currency: String,   
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Status {
    delivery_date: DateTime<Utc>,
    value: String,
    lookup: Option<NumberLookup>,
}