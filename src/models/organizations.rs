use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use url::Url;
use uuid::Uuid;

use super::{
    Price, 
    Links
};

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct OrganizationList {
    page: u16,
    page_size: u32,
    count: u32,
    total_count: u32,
    links: Links,
    items: Vec<OrganizationDetails>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct OrganizationDetails {
    organization_id: Uuid,
    name: String,
    state: Option<State>,
    balance: Price,
    creation_date: DateTime<Utc>,
    href: Url, 
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum State {
    Activated,
    NotActivated,
    Blocked,
}