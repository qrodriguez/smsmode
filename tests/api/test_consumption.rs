use chrono::{Utc, Duration};
use serde_test::{assert_ser_tokens, Token};
use sms_mode::{api::commons::consumption::{ConsumptionsListRequest, ConsumptionDetailsRequest}, Endpoint};
use uuid::Uuid;

// #[test]
// fn test_consumptions_list_serialize() {
//     let consumptions_list = ConsumptionsListRequest::new();

//     println!("{:?}", consumptions_list);


//     assert_ser_tokens(&consumptions_list, &[
//         Token::Struct { name: "ConsumptionsListRequest", len: 4 },

//         Token::Str("startDate"),
//         Token::NewtypeStruct { name: "DateTime" },

//         Token::Str("endDate"),
//         Token::BorrowedStr(Utc::now().to_string()),

//         Token::Str("page"),
//         Token::U16(1),

//         Token::Str("pageSize"),
//         Token::U32(10),

//         Token::StructEnd,
//     ])
// }


#[test]
fn test_consumption_details_without_search() {
    let consumption_details = ConsumptionDetailsRequest::new(
        Uuid::parse_str("da0e501d-4449-40b1-b1f9-3cd1e94031bd").unwrap()
    );

    assert_ser_tokens(&consumption_details, &[
        Token::Struct { name: "ConsumptionDetailsRequest", len: 2 },
        Token::Str("groupBy"),
        Token::String("mccMnc"),
        Token::Str("detailStatuses"),
        Token::Bool(false),
        Token::StructEnd,
    ])
}

#[test]
fn test_consumption_details_with_search_serialize() {
    let mut  consumption_details = ConsumptionDetailsRequest::new(
        Uuid::parse_str("da0e501d-4449-40b1-b1f9-3cd1e94031bd").unwrap()
    );

    consumption_details.search_by.insert(
        "type".to_string(), 
        "SMS_MOUNTH".to_string()
    );

    assert_ser_tokens(&consumption_details, &[
        Token::Struct { name: "ConsumptionDetailsRequest", len: 3 },
        Token::Str("searchBy"),
        Token::Map { len: Some(1) },
        Token::Str("type"),
        Token::String("SMS_MOUNTH"),
        Token::MapEnd,
        Token::Str("groupBy"),
        Token::String("mccMnc"),
        Token::Str("detailStatuses"),
        Token::Bool(false),
        Token::StructEnd,
    ])
}

#[test]
fn test_consumption_details_path() {
    let consumption_details = ConsumptionDetailsRequest::new(
        Uuid::parse_str("da0e501d-4449-40b1-b1f9-3cd1e94031bd").unwrap()
    );
    
    assert_eq!(consumption_details.path(), "/consumptions/da0e501d-4449-40b1-b1f9-3cd1e94031bd")
}