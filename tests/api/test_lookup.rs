use serde_test::{Token, assert_ser_tokens};
use sms_mode::{
    Endpoint,
    api::commons::lookup::NumberLookupRequest, 
};

#[test]
fn test_lookup_serialize() {
    let lookup = NumberLookupRequest::new(
        "336XXXXXXXX".to_owned(),
    );

    assert_ser_tokens(&lookup, &[
        Token::Struct { name: "NumberLookupRequest", len: 0 },
        Token::StructEnd,
    ])
}


#[test]
fn test_lookup_path() {
    let lookup = NumberLookupRequest::new(
        "336XXXXXXXX".to_owned(),
    );

    assert_eq!(lookup.path(), "/lookup/336XXXXXXXX")
}
