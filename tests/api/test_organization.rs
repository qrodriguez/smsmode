use serde_test::{Token, assert_ser_tokens};
use sms_mode::{
    api::commons::organization::{
        OrganizationsListRequest, 
        OrganizationDetailsRequest
    }, 
    Endpoint
};

use uuid::Uuid;


#[test]
fn test_organization_list_serialize() {
    let organization_list = OrganizationsListRequest::new();

    assert_ser_tokens(&organization_list, &[
        Token::Struct { name: "OrganizationsListRequest", len: 2 },
        Token::Str("page"),
        Token::U16(1),
        Token::Str("pageSize"),
        Token::U32(10),
        Token::StructEnd,
    ])
}

#[test]
fn test_organization_details_serialize() {
    let organization_details = OrganizationDetailsRequest::new(
        Uuid::parse_str("da0e501d-4449-40b1-b1f9-3cd1e94031bd").unwrap()
    );

    assert_ser_tokens(&organization_details, &[
        Token::Struct { name:"OrganizationDetailsRequest", len: 0 },
        Token::StructEnd,
    ])
}

#[test]
fn test_organization_details_path() {
    let organization_details = OrganizationDetailsRequest::new(
        Uuid::parse_str("da0e501d-4449-40b1-b1f9-3cd1e94031bd").unwrap()
    );
    
    assert_eq!(organization_details.path(), "/organisations/da0e501d-4449-40b1-b1f9-3cd1e94031bd")
}
